import React from "react";
import { MessageSquare, Minimize } from "react-feather";
import { translate } from "react-switch-lang";

import Notification from "../../components/header/notifications";
import Messages from "../../components/header/messages";
import DarkMode from "../../components/header/dark_mode";
import Profile from "../../components/header/profile";
import LangSelector from "../../components/header/lang_selector";
import SearchTrigger from "../../components/header/search_trigger";

const Rightbar = (props) => {
  //full screen function
  function goFull() {
    if (
      (document.fullScreenElement && document.fullScreenElement !== null) ||
      (!document.mozFullScreen && !document.webkitIsFullScreen)
    ) {
      if (document.documentElement.requestFullScreen) {
        document.documentElement.requestFullScreen();
      } else if (document.documentElement.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen();
      } else if (document.documentElement.webkitRequestFullScreen) {
        document.documentElement.webkitRequestFullScreen(
          Element.ALLOW_KEYBOARD_INPUT
        );
      }
    } else {
      if (document.cancelFullScreen) {
        document.cancelFullScreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
      }
    }
  }

  return (
    <>
      <div className="nav-right col-8 pull-right right-menu">
        <ul className="nav-menus">
          {/* <li className="language-nav">
            <LangSelector />
          </li> */}
          {/* <li>
            <SearchTrigger />
          </li> */}
          {/* <li className="onhover-dropdown">
            <Notification />
          </li> */}
          <li>
            <DarkMode />
          </li>
          {/* <li className="onhover-dropdown">
            <MessageSquare />
            <Messages />
          </li> */}
          <li className="maximize">
            <a className="text-dark" href="#javascript" onClick={goFull}>
              <Minimize />
            </a>
          </li>
          <li className="onhover-dropdown p-0">
            <Profile />
          </li>
        </ul>
      </div>
    </>
  );
};
export default translate(Rightbar);
