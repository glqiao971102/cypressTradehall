import React from "react";
import { Container, Row, Col } from "reactstrap";

const Footer = (props) => {
  return (
    <>
      <footer className="footer">
        <Container fluid={true}>
          <Row>
            <Col md="6" className="footer-copyright">
              <p className="mb-0">Support 24/7</p>
              <p className="mb-0">+60 12 345 1234</p>
            </Col>
            <Col md="6" style={{ display: "flex", justifyContent: "flex-end", alignItems: "center"}}>
              <p className="mb-0">
                Copyright © Tradehall 2020
              </p>
            </Col>
          </Row>
        </Container>
      </footer>
    </>
  );
};

export default Footer;
