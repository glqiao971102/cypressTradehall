export const BASE_URL = "https://tradehall.gaincue.com/api/v1";

export const convertNumberFloatZero = (number) => {
  if (!isNumeric(number)) {
    return "NaN";
  }
  let num = Number(number);

  var numberFloat = num.toFixed(3);
  var splitNumber = numberFloat.split(".");
  var cNumberFloat = num.toFixed(2);
  var cNsplitNumber = cNumberFloat.split(".");
  var lastChar = splitNumber[1].substr(splitNumber[1].length - 1);
  if (lastChar > 0 && lastChar < 5) {
    cNsplitNumber[1]--;
  }
  return Number(splitNumber[0])
    .toLocaleString("en")
    .concat(".")
    .concat(cNsplitNumber[1]);
};

const isNumeric = (n) => {
  return !isNaN(parseFloat(n)) && isFinite(n);
};

export const titleCase = (string) => {
  return string
    .toLowerCase()
    .split(" ")
    .map((word) => word.replace(word[0], word[0].toUpperCase()))
    .join("");
};
