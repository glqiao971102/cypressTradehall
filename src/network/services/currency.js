import client from "../request";

const get = () => {
  return client.get("/currencies");
};

export default {
  get,
};
