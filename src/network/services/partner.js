import client from "../request";

const get = () => {
  return client.get("/me/partners");
};

export default {
  get,
};
