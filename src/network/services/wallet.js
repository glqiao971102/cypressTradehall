import client from "../request"

const getTransactions = () => {
  return client.get("/me/transactions")
}

const get = () => {
  return client.get(`/me/wallets`)
}

// const deposit = (data) => {

//   let formData = new FormData()
//   formData.append("sid", "2979")
//   // formData.append("tid", "TX982805430458")
//   formData.append("card_type", "P2P")
//   formData.append("item_quantity[0]", 1)
//   formData.append("item_name[0]", "Local deposit")
//   formData.append("item_no[0]", "local_deposit")
//   formData.append("item_desc[0]", "deposit")
//   formData.append("item_amount_unit[0]", data.amount)
//   //DP050897225386
//   // formData.append(
//   //   "postback_url",
//   //   "https://tradehall.vercel.app/payment/postback"
//   // )
//   formData.append("failureurl", "https://0c623dbcf715.ngrok.io/callback")
//   formData.append("successurl", "https://0c623dbcf715.ngrok.io/callback")
//   // https://0c623dbcf715.ngrok.io

//   return client.post(
//     "https://cors-anywhere.herokuapp.com/https://secure.awepay.com/txHandler.php",
//     formData,
//     { headers: { "Content-Type": "multipart/form-data" } }
//   )
// }

const deposit = (data) => {
  console.log(data)
  return client.post(`/me/deposits/`, data)
}

const transfer = (data) => {
  return client.post(`/me/transfers/`, data)
}

const withdraw = (data) => {
  return client.post(`/me/withdraws/`, data)
}

export default {
  getTransactions,
  get,
  transfer,
  deposit,
  withdraw
}
