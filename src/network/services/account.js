import client from "../request";

const get = () => {
  return client.get("/me/accounts");
};

export default {
  get,
};
