import { SET_USER, SET_AGENT } from "../actionTypes";

const initial_state = {
  user: null,
  partners: null,
};

export default (state = initial_state, action) => {
  switch (action.type) {
    case SET_USER:
      return { ...state, user: action.user };

    case SET_AGENT:
      return { ...state, partners: action.partners };

    default:
      return { ...state };
  }
};
