import { combineReducers } from "redux";
// import Customizer from "./customizer/reducer";
import user from "./user/reducer";
import header from "./header/reducer";
import monitoring from "./monitoring/reducer";
import wallet from "./wallet/reducer";
import account from "./account/reducer";
import currency from "./currency/reducer";

const reducers = combineReducers({
  // Customizer,
  user,
  header,
  monitoring,
  wallet,
  account,
  currency,
});

export default reducers;
