import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";

import * as serviceWorker from "./serviceWorker";
import "./index.scss";

import store from "./store";
import ScrollToTop from "./layout/scroll_to_top";
import AppWrapper from "./app_wrapper";

const Root = (props) => {
  const abortController = new AbortController();

  useEffect(() => {
    return function cleanup() {
      abortController.abort();
    };
    // eslint-disable-next-line
  }, []);

  return (
    <>
      <Provider store={store}>
        <BrowserRouter basename={`/`}>
          <ScrollToTop />
          <AppWrapper />
        </BrowserRouter>
      </Provider>
    </>
  );
};

ReactDOM.render(<Root />, document.getElementById("root"));

serviceWorker.unregister();
