import React, { useState } from "react";
import { Card, Button, CardHeader, CardBody, Input } from "reactstrap";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { toast } from "react-toastify";

const Referral = () => {
  const [link, setLink] = useState("abcdefg");

  return (
    <>
      <Card>
        <CardHeader>
          <h5>Referral Link</h5>
        </CardHeader>
        <CardBody>
          <div style={{ display: "flex" }}>
            <Input className="form-control" name="referral_link" value={link} />
            <CopyToClipboard text={link}>
              <Button
                color="primary"
                className="notification"
                onClick={() =>
                  toast.success("Link copied to clipboard!", {
                    position: toast.POSITION.BOTTOM_RIGHT,
                  })
                }
                style={{ marginLeft: 12 }}
              >
                Copy
              </Button>
            </CopyToClipboard>
          </div>
        </CardBody>
      </Card>
    </>
  );
};

export default Referral;
