import React from "react";
import DataTable from "react-data-table-component";

const columns = [
  {
    name: "Clients",
    selector: "name",
    sortable: true,
  },
  {
    name: "Wallet Deposits",
    selector: "account",
    sortable: true,
  },
  {
    name: "Wallet Withdrawals",
    selector: "level",
    sortable: true,
  },
  {
    name: "Volume",
    selector: "ib",
    sortable: true,
    right: true,
  },
  {
    name: "Commission",
    selector: "deposit",
    sortable: true,
    right: true,
  },
  {
    name: "Country",
    selector: "withdraw",
    sortable: true,
    right: true,
  },
  {
    name: "Registration Date",
    selector: "lots",
    sortable: true,
  },
];

const ClientTable = () => {
  return (
    <DataTable
      noHeader
      data={null}
      columns={columns}
      striped={true}
      center={true}
    />
  );
};

export default ClientTable;
