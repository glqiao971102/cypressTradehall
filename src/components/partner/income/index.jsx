import React from "react";
import { Card, CardBody } from "reactstrap";
import IncomeTable from "./table";

const Income = () => {
  return (
    <Card>
      <CardBody>
        <h5>No income data</h5>
        {/* <IncomeTable /> */}
      </CardBody>
    </Card>
  );
};

export default Income;
