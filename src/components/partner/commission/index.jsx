import React from "react";
import { Card, CardBody } from "reactstrap";
import CommissionTable from "./table";

const Commissions = () => {
  return (
    <Card>
      <CardBody>
        <h5>No commissions data</h5>
        {/* <CommissionTable /> */}
      </CardBody>
    </Card>
  );
};

export default Commissions;
