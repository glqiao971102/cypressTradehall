import React, { useState } from "react";
import DataTable from "react-data-table-component";
import moment from "moment";

const tableData = [
  {
    id: 1,
    status: "Ongoing",
    subject: "1",
    date: moment().format("yyyy-mm-DD hh:MMA"),
  },
];

const columns = [
  {
    name: "Ticket",
    selector: "id",
    sortable: true,
  },
  {
    name: "Status",
    selector: "status",
    sortable: true,
  },
  {
    name: "Subject",
    selector: "subject",
    sortable: true,
  },
  {
    name: "Updated",
    selector: "date",
    sortable: true,
  },
];

const TicketTable = () => {
  const [data, setData] = useState(tableData);

  return (
    <DataTable
      title="Tickets"
      data={data}
      columns={columns}
      striped={true}
      center={true}
    />
  );
};

export default TicketTable;
