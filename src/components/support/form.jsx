import React, { useState } from "react";
import { Row, Col, Form, FormGroup, Label, Input, Button } from "reactstrap";
import { useForm } from "react-hook-form";
import Dropzone from "react-dropzone-uploader";

const SupportForm = () => {
  const { register, handleSubmit, errors } = useForm();
  const [url, setUrl] = useState("");

  const handleSupport = (data) => {
    if (data !== "") {
    } else {
      errors.showMessages();
    }
  };

  const getUploadParams = ({ meta }) => {
    setUrl(meta.previewUrl);
    return {
      url: "https://httpbin.org/post",
    };
  };

  const handleChangeStatus = ({ meta, file }, status) => {};
  return (
    <Form className="theme-form" onSubmit={handleSubmit(handleSupport)}>
      <Row>
        <Col>
          <FormGroup>
            <Label>Ticket Type</Label>
            <Input
              type="select"
              name="type"
              className="form-control digits"
              innerRef={register({ required: true })}
            >
              <option value="common">Common Questions</option>
              <option value="payment">Payments</option>
              <option value="partnership">Partnership</option>
              <option value="complaint">Complaint</option>
            </Input>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup>
            <Label>Subject</Label>
            <Input
              className="form-control"
              type="text"
              name="subject"
              innerRef={register({ required: true })}
            />
            <span style={{ color: "red" }}>
              {errors.subject && "Subject is required"}
            </span>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup>
            <Label>Message</Label>
            <Input
              className="form-control"
              type="text"
              name="message"
              innerRef={register({ required: true })}
            />
            <span style={{ color: "red" }}>
              {errors.message && "Message is required"}
            </span>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup>
            <Label>Attachement</Label>
            <Dropzone
              getUploadParams={getUploadParams}
              onChangeStatus={handleChangeStatus}
              maxFiles={1}
              multiple={false}
              canCancel={true}
              inputContent="Drop A File"
              styles={{
                dropzone: { width: "100%", height: 50 },
                dropzoneActive: { borderColor: "green" },
              }}
            />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup className="mb-0">
            <Button color="success" className="mr-3">
              Confirm
            </Button>
          </FormGroup>
        </Col>
      </Row>
    </Form>
  );
};

export default SupportForm;
