import React, { useState } from "react";
import WalletOption from "../options";
import WalletHistory from "./wallet";
import MT5History from "./mt5";

const History = () => {
  const [option, setOption] = useState();

  return (
    <>
      {option == null ? (
        <WalletOption setOption={setOption} />
      ) : (
        <>
          {option === 1 ? (
            <WalletHistory setOption={setOption} />
          ) : (
            <MT5History setOption={setOption} />
          )}
        </>
      )}
    </>
  );
};

export default History;
