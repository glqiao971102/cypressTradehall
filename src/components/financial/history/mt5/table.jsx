import React, { useCallback, useState, useMemo } from "react";
import DataTable from "react-data-table-component";
import moment from "moment";
import { toast } from "react-toastify";
import differenceBy from "lodash/differenceBy";

const tableData = [
  {
    id: 321247,
    date: moment().format("yyyy-MM-DD hh:mmA"),
    wallet: "USD",
    action: "Deposit",
    method: "Crypto",
    amount: 1,
    status: "rejected",
  },
];

const columns = [
  {
    name: "Ticket",
    selector: "id",
    sortable: true,
  },
  {
    name: "Date",
    selector: "date",
    sortable: true,
  },
  {
    name: "Wallet",
    selector: "wallet",
    sortable: true,
  },
  {
    name: "Action",
    selector: "action",
    sortable: true,
  },
  {
    name: "Method",
    selector: "method",
    sortable: true,
  },
  {
    name: "Amount",
    selector: "amount",
    sortable: true,

    right: true,
  },
  {
    name: "Status",
    selector: "status",
    sortable: true,
  },
];

const HistoryTable = () => {
  //   const [selectedRows, setSelectedRows] = useState([]);
  //   const [toggleCleared, setToggleCleared] = useState(false);
  const [data, setData] = useState(tableData);

  //   const handleRowSelected = useCallback((state) => {
  //     setSelectedRows(state.selectedRows);
  //   }, []);

  //   const contextActions = useMemo(() => {
  //     const handleDelete = () => {
  //       if (
  //         window.confirm(
  //           `Are you sure you want to delete:\r ${selectedRows.map(
  //             (r) => r.name
  //           )}?`
  //         )
  //       ) {
  //         setToggleCleared(!toggleCleared);
  //         setData(differenceBy(data, selectedRows, "name"));
  //         toast.success("Successfully Deleted !");
  //       }
  //     };

  //     return (
  //       <button key="delete" className="btn btn-danger" onClick={handleDelete}>
  //         Delete
  //       </button>
  //     );
  //   }, [data, selectedRows, toggleCleared]);

  return (
    <DataTable
      noHeader
      data={data}
      columns={columns}
      striped={true}
      center={true}
      //   selectableRows
      //   persistTableHead
      //   contextActions={contextActions}
      //   onSelectedRowsChange={handleRowSelected}
      //   clearSelectedRows={toggleCleared}
    />
  );
};

export default HistoryTable;
