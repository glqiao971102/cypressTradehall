import React, { useState } from "react";
import WalletOption from "../options";
import WalletWithdrawal from "./wallet";
import MT5Withdrawal from "./mt5";

const Withdrawal = () => {
  const [option, setOption] = useState();

  return (
    <>
      {option == null ? (
        <WalletOption setOption={setOption} />
      ) : (
        <>
          {option === 1 ? (
            <WalletWithdrawal setOption={setOption} />
          ) : (
            <MT5Withdrawal setOption={setOption} />
          )}
        </>
      )}
    </>
  );
};

export default Withdrawal;
