import React, { useState } from "react";
import { Row, Form, Col, Card, CardBody, Button } from "reactstrap";
import { useForm } from "react-hook-form";
import StepZilla from "react-stepzilla";
import SelectCurrency from "./currency";

import SelectPaymentMethod from "./payment";
import ChooseAmount from "./amount";

const MT5Withdrawal = ({ setOption }) => {
  const [stage, setStage] = useState(0);
  const { handleSubmit, errors } = useForm();
  const handleDeposit = (data) => {
    if (data !== "") {
    } else {
      errors.showMessages();
    }
  };

  return (
    <>
      <Card>
        <CardBody>
          <Row>
            <Col
              className={`u-pearl col-4 ${stage === 0 && "current"} ${
                stage > 0 && "done"
              }`}
            >
              <span className="u-pearl-number">1</span>
              <span className="u-pearl-title">Select Your Account</span>
            </Col>
            <Col
              className={`u-pearl col-4 ${stage === 1 && "current"} ${
                stage > 1 && "done"
              }`}
            >
              <span className="u-pearl-number">2</span>
              <span className="u-pearl-title">Select Wallet</span>
            </Col>
            <Col className={`u-pearl col-4 ${stage === 2 && "current"}`}>
              <span className="u-pearl-number">3</span>
              <span className="u-pearl-title">Choose Amount</span>
            </Col>
          </Row>
        </CardBody>
      </Card>

      <Form className="theme-form" onSubmit={handleSubmit(handleDeposit)}>
        <StepZilla
          steps={[
            {
              name: "Step 1",
              component: <SelectCurrency />,
            },
            {
              name: "Step 2",
              component: <SelectPaymentMethod />,
            },
            {
              name: "Step 3",
              component: <ChooseAmount />,
            },
          ]}
          showSteps={false}
          onStepChange={(index) => {
            setStage(index);
          }}
        />
      </Form>

      {stage === 0 && (
        <Button
          color="primary"
          onClick={() => {
            setOption(null);
          }}
        >
          Back
        </Button>
      )}
    </>
  );
};

export default MT5Withdrawal;
