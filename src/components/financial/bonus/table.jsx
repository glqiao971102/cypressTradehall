import React, { useCallback, useState, useMemo } from "react";
import DataTable from "react-data-table-component";
import moment from "moment";
import { toast } from "react-toastify";
import differenceBy from "lodash/differenceBy";

const tableData = [
  {
    id: 321247,
    account: "12345",
    date: moment().format("yyyy-MM-DD hh:mmA"),
    amount: 1,
    bonus: 1,
    equity: 1,
    status: "rejected",
    reason: "rejected",
  },
];

const columns = [
  {
    name: "Ticket",
    selector: "id",
    sortable: true,
  },
  {
    name: "Account",
    selector: "account",
    sortable: true,
  },
  {
    name: "Date",
    selector: "date",
    sortable: true,
  },
  {
    name: "Amount",
    selector: "amount",
    sortable: true,
    right: true,
  },
  {
    name: "Bonus",
    selector: "bonus",
    sortable: true,
    right: true,
  },
  {
    name: "Equity",
    selector: "equity",
    sortable: true,
    right: true,
  },
  {
    name: "Status",
    selector: "status",
    sortable: true,
  },
  {
    name: "Reason",
    selector: "reason",
    sortable: true,
  },
];

const BonusTable = () => {
  //   const [selectedRows, setSelectedRows] = useState([]);
  //   const [toggleCleared, setToggleCleared] = useState(false);
  const [data, setData] = useState(tableData);

  //   const handleRowSelected = useCallback((state) => {
  //     setSelectedRows(state.selectedRows);
  //   }, []);

  //   const contextActions = useMemo(() => {
  //     const handleDelete = () => {
  //       if (
  //         window.confirm(
  //           `Are you sure you want to delete:\r ${selectedRows.map(
  //             (r) => r.name
  //           )}?`
  //         )
  //       ) {
  //         setToggleCleared(!toggleCleared);
  //         setData(differenceBy(data, selectedRows, "name"));
  //         toast.success("Successfully Deleted !");
  //       }
  //     };

  //     return (
  //       <button key="delete" className="btn btn-danger" onClick={handleDelete}>
  //         Delete
  //       </button>
  //     );
  //   }, [data, selectedRows, toggleCleared]);

  return (
    <DataTable
      noHeader
      data={data}
      columns={columns}
      striped={true}
      center={true}
      //   selectableRows
      //   persistTableHead
      //   contextActions={contextActions}
      //   onSelectedRowsChange={handleRowSelected}
      //   clearSelectedRows={toggleCleared}
    />
  );
};

export default BonusTable;
