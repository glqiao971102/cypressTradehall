import React, { useState } from "react";
import WalletOption from "../options";
import WalletDeposit from "./wallet";
import MT5Deposit from "./mt5";

const Deposit = () => {
  const [option, setOption] = useState();

  return (
    <>
      {option == null ? (
        <WalletOption setOption={setOption} />
      ) : (
        <>
          {option === 1 ? (
            <WalletDeposit setOption={setOption} />
          ) : (
            <MT5Deposit setOption={setOption} />
          )}
        </>
      )}
    </>
  );
};

export default Deposit;
