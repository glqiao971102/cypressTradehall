import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { useFormContext } from "react-hook-form";

const SelectCurrency = () => {
  const { register } = useFormContext();

  return (
    <>
      <Card>
        <CardHeader>
          <h5>Select Currency</h5>
        </CardHeader>
        <CardBody>
          <FormGroup>
            <Label>Wallet Currency</Label>
            <Input
              type="select"
              name="currency"
              className="form-control digits"
              innerRef={register({ required: true })}
            >
              <option value="usd">USD Available Balance: 0.00</option>
            </Input>
          </FormGroup>
        </CardBody>
      </Card>
    </>
  );
};

export default SelectCurrency;
