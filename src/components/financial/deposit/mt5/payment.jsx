import React from "react";
import { Button, Card, CardHeader, CardBody, Input } from "reactstrap";
import DataTable from "react-data-table-component";

const data = [
  {
    id: "1",
    description: "Bank Wire, withdrawal within 1-24 hours",
    duration: "1-24 hours",
    fee: 0,
  },
];

const SelectPaymentMethod = () => {
  const columns = [
    {
      name: "Payment Method",
      selector: "id",
    },
    {
      name: "Description",
      selector: "description",
    },
    {
      name: "Funding Time",
      selector: "duration",
    },
    {
      name: "Fee",
      selector: "fee",
    },
    {
      name: "",
      button: true,
      width: "120px",
      cell: (row) => (
        <Button color="primary" onClick={() => {}}>
          Select
        </Button>
      ),
    },
  ];

  return (
    <>
      <Card>
        <CardHeader>
          <h5>Select Payment Method</h5>
        </CardHeader>
        <CardBody>
          <div className="table-responsive product-table">
            <DataTable noHeader columns={columns} data={data} />
          </div>
          <Input type="text" name="method" style={{ display: "none" }} />
        </CardBody>
      </Card>
    </>
  );
};

export default SelectPaymentMethod;
