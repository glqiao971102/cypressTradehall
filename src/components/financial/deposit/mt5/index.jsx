import React, { useState } from "react";
import { Row, Form, Col, Card, CardBody, Button } from "reactstrap";
import { useForm, FormProvider } from "react-hook-form";
import StepZilla from "react-stepzilla";

import SelectCurrency from "./currency";
import SelectPaymentMethod from "./payment";
import ChooseAmount from "./amount";
import WalletService from "../../../../network/services/wallet";

const MT5Deposit = ({ setOption }) => {
  const [stage, setStage] = useState(0);
  const methods = useForm();
  const handleDeposit = async (data) => {
    console.log(data);
    if (data !== "") {
      let result = await WalletService.deposit(data);

      try {
        result = result.replace("</form>", "</form>sp()");
        let array = result.split("sp()");

        const parent = document.createElement("div");
        parent.innerHTML = array[0];
        document.getElementById("payment_callback").append(parent);
        parent.firstChild.submit();
      } catch (error) {
        console.log(error);
      }
    } else {
      methods.errors.showMessages();
    }
  };

  return (
    <>
      <Card>
        <CardBody>
          <Row>
            <Col
              className={`u-pearl col-4 ${stage === 0 && "current"} ${
                stage > 0 && "done"
              }`}
            >
              <span className="u-pearl-number">1</span>
              <span className="u-pearl-title">Select Your MT5 Account</span>
            </Col>
            <Col
              className={`u-pearl col-4 ${stage === 1 && "current"} ${
                stage > 1 && "done"
              }`}
            >
              <span className="u-pearl-number">2</span>
              <span className="u-pearl-title">Choose Wallet Currency</span>
            </Col>
            <Col className={`u-pearl col-4 ${stage === 2 && "current"}`}>
              <span className="u-pearl-number">3</span>
              <span className="u-pearl-title">Choose Amount</span>
            </Col>
          </Row>
        </CardBody>
      </Card>

      <FormProvider {...methods}>
        <Form
          className="theme-form"
          onSubmit={methods.handleSubmit(handleDeposit)}
        >
          <StepZilla
            steps={[
              {
                name: "Step 1",
                component: <SelectCurrency />,
              },
              {
                name: "Step 2",
                component: <SelectPaymentMethod />,
              },
              {
                name: "Step 3",
                component: <ChooseAmount />,
              },
            ]}
            showSteps={false}
            onStepChange={(index) => {
              setStage(index);
            }}
          />
        </Form>
      </FormProvider>

      {stage === 0 && (
        <Button
          color="primary"
          onClick={() => {
            setOption(null);
          }}
        >
          Back
        </Button>
      )}

      <div id="payment_callback" style={{ display: "hidden" }}></div>
    </>
  );
};

export default MT5Deposit;
