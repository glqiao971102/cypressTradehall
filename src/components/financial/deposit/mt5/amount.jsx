import React from "react";
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";
import { useFormContext } from "react-hook-form";

const ChooseAmount = () => {
  const { register, errors } = useFormContext();

  return (
    <>
      <Card>
        <CardHeader>
          <h5>Choose Amount</h5>
        </CardHeader>
        <CardBody>
          <Row>
            <Col>
              <FormGroup>
                <Label>Deposit Amount</Label>
                <Input
                  className="form-control"
                  type="number"
                  name="amount"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: "red" }}>
                  {errors.amount && "Amount is required"}
                </span>
                <p />
                <span style={{ color: "red" }}>
                  Warning: Make sure Your card/account have the enough balance
                  and all the details are valid. Three continuous transactions
                  failure could block your card.
                </span>
              </FormGroup>
              <FormGroup>
                <div className="custom-control custom-checkbox mb-3">
                  <Input
                    className="custom-control-input"
                    id="terms_validation"
                    type="checkbox"
                    required
                  />
                  <Label
                    className="custom-control-label"
                    htmlFor="terms_validation"
                  >
                    I have read all instructions and agree with terms and
                    conditions of payments operations
                  </Label>
                  <div className="invalid-feedback">
                    Please agree to the terms
                  </div>
                </div>
              </FormGroup>
              <Button
                color="primary btn-block"
                type="submit"
                style={{ maxWidth: 150, float: "right" }}
              >
                Submit
              </Button>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </>
  );
};

export default ChooseAmount;
