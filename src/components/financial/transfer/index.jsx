import React from "react";
import { Card, CardBody } from "reactstrap";
import TransferForm from "./form";

const Transfer = () => {
  return (
    <Card>
      <CardBody>
        <TransferForm />
      </CardBody>
    </Card>
  );
};

export default Transfer;
