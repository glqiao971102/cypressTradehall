import React from "react";
import { Row, Col, Form, FormGroup, Label, Input, Button } from "reactstrap";
import { useForm } from "react-hook-form";

const TransferForm = () => {
  const { register, handleSubmit, errors } = useForm();

  const handleTransfer = (data) => {
    if (data !== "") {
    } else {
      errors.showMessages();
    }
  };

  return (
    <Form className="theme-form" onSubmit={handleSubmit(handleTransfer)}>
      <Row>
        <Col>
          <FormGroup>
            <Label>Wallet Currency</Label>
            <Input
              type="select"
              name="currency"
              className="form-control digits"
              innerRef={register({ required: true })}
            >
              <option value="usd">USD Available Balance: 0.00</option>
            </Input>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup>
            <Label>Client Area Password</Label>
            <Input
              className="form-control"
              type="password"
              name="password"
              innerRef={register({ required: true })}
            />
            <span style={{ color: "red" }}>
              {errors.password && "Password is required"}
            </span>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup>
            <Label>Transfer Amount</Label>
            <Input
              className="form-control"
              type="number"
              name="amount"
              innerRef={register({ required: true })}
            />
            <span style={{ color: "red" }}>
              {errors.amount && "Amount is required"}
            </span>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup>
            <Label>Wallet Number (recipient)</Label>
            <Input
              className="form-control"
              type="number"
              name="recipient"
              innerRef={register({ required: true })}
            />
            <span style={{ color: "red" }}>
              {errors.recipient && "Wallet is required"}
            </span>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup>
            <Label>To Wallet Account (Email)</Label>
            <Input
              className="form-control"
              type="email"
              name="receiver"
              innerRef={register({ required: true })}
            />
            <span style={{ color: "red" }}>
              {errors.receiver && "Email is required"}
            </span>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup className="mb-0">
            <Button color="success" className="mr-3">
              Confirm
            </Button>
          </FormGroup>
        </Col>
      </Row>
    </Form>
  );
};

export default TransferForm;
