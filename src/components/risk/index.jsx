import React from "react";
import { Container, Row, Col, Card, CardHeader, CardBody } from "reactstrap";

import Breadcrumb from "../../layout/breadcrumb";
import RiskTable from "./table";

const RiskManagement = (props) => {
  return (
    <>
      <Breadcrumb title="Risk Management" />
      <Container fluid={true}>
        <Row>
          <Col sm="12">
            <Card>
              <CardHeader>
                <h5>Risk Management</h5>
              </CardHeader>
              <CardBody>
                <RiskTable />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default RiskManagement;
