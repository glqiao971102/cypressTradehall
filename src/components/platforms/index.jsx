import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  Media,
  CardBody,
} from "reactstrap";
import styled from "styled-components";

import Breadcrumb from "../../layout/breadcrumb";

const HoverCard = styled(Card)`
  cursor: pointer;
  &:hover {
    transform: scale(1.1);
    transition: 0.2s;
  }
`;

const Platform = () => {
  return (
    <>
      <Breadcrumb title={"Platforms"} />
      <Container fluid={true}>
        <Card>
          <CardBody>
            <h5>Get your MetaTrader 5</h5>
            <p>
              One of the most powerful trading platforms for Windows, OS X and
              mobile devices
            </p>
          </CardBody>
        </Card>
        <Row>
          <Col sm="3">
            <HoverCard>
              <CardBody>
                <Media>
                  <i
                    className="icofont icofont-brand-windows"
                    style={{ fontSize: 30 }}
                  ></i>
                  <Media body>
                    <h6 style={{ fontSize: 23 }}>
                      <span className="badge badge-primary pull-right">
                        Windows
                      </span>
                    </h6>
                  </Media>
                </Media>
              </CardBody>
            </HoverCard>
          </Col>
          <Col sm="3">
            <HoverCard>
              <CardBody>
                <Media>
                  <i
                    className="icofont icofont-brand-android-robot"
                    style={{ fontSize: 30 }}
                  ></i>
                  <Media body>
                    <h6 style={{ fontSize: 23 }}>
                      <span className="badge badge-primary pull-right">
                        Android
                      </span>
                    </h6>
                  </Media>
                </Media>
              </CardBody>
            </HoverCard>
          </Col>
          <Col sm="3">
            <HoverCard>
              <CardBody>
                <Media>
                  <i
                    className="icofont icofont-brand-apple"
                    style={{ fontSize: 30 }}
                  ></i>
                  <Media body>
                    <h6 style={{ fontSize: 23 }}>
                      <span className="badge badge-primary pull-right">
                        iOS
                      </span>
                    </h6>
                  </Media>
                </Media>
              </CardBody>
            </HoverCard>
          </Col>
          <Col sm="3">
            <HoverCard>
              <CardBody>
                <Media>
                  <i
                    className="icofont icofont-monitor"
                    style={{ fontSize: 30 }}
                  ></i>
                  <Media body>
                    <h6 style={{ fontSize: 23 }}>
                      <span className="badge badge-primary pull-right">
                        Web
                      </span>
                    </h6>
                  </Media>
                </Media>
              </CardBody>
            </HoverCard>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Platform;
