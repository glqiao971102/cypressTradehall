import React, { useState, useEffect } from "react";
import {
  Form,
  FormGroup,
  Label,
  Input,
  Card,
  CardBody,
  Button,
  Table,
} from "reactstrap";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";

import UserService from "../../../network/services/user";

const UserDetail = () => {
  const [isEditable, setIsEditable] = useState(false);
  const currentUser = useSelector((state) => state.user.user);
  const partners = useSelector((state) => state.user.partners);
  const { register, handleSubmit, errors } = useForm();
  const submitUpdate = async (data) => {
    try {
      const result = await UserService.update(currentUser.id, {
        bank: "",
      });

      console.log(result);
    } catch (error) {
      console.log(error);
      // if (
      //   error.message === "E_INVALID_AUTH_PASSWORD" ||
      //   error.message === "E_INVALID_AUTH_UID"
      // ) {
      //   setLoginError("Invalid credential");
      // } else {
      //   setLoginError(error.message ?? "Please try again later");
      // }
    }
  };

  console.log(partners)

  return (
    <Card>
      <CardBody>
        <Table borderless>
          <tbody>
            <tr>
              <td width="50%">Name</td>
              <td>{currentUser?.full_name ?? "-"}</td>
            </tr>
            <tr>
              <td>Email</td>
              <td>{currentUser?.email ?? "-"}</td>
            </tr>
            <tr>
              <td>Address</td>
              <td>{currentUser?.address ?? "-"}</td>
            </tr>
            <tr>
              <td>Phone number</td>
              <td>{currentUser?.phone ?? "-"}</td>
            </tr>
            <tr>
              <td>IB Account</td>
              <td>{partners?.map((partner) => partner.ib_code)?.join(",") ?? "-"}</td>
            </tr>
            <tr>
              <td style={{ verticalAlign: "middle" }}>Bank Details</td>
              <td>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <span>{currentUser?.bank_details ?? "-"}</span>
                  <Button
                    onClick={() => {
                      setIsEditable(!isEditable);
                    }}
                  >
                    {isEditable ? "Cancel" : "Edit"}
                  </Button>
                </div>
              </td>
            </tr>
          </tbody>
        </Table>
        {isEditable && (
          <Form
            className="theme-form"
            onSubmit={handleSubmit(submitUpdate)}
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              padding: "0.75rem",
            }}
          >
            <h4>Edit Bank Details</h4>
            <FormGroup>
              <Label className="col-form-label pt-0">Account Holder</Label>
              <Input
                className="form-control"
                type="text"
                required=""
                name="name"
                innerRef={register({ required: true })}
              />
              <span style={{ color: "red" }}>
                {errors.name && "Name is required"}
              </span>
            </FormGroup>
            <FormGroup>
              <Label className="col-form-label pt-0">Bank Branch</Label>
              <Input
                className="form-control"
                type="text"
                required=""
                name="branch"
                innerRef={register({ required: true })}
              />
              <span style={{ color: "red" }}>
                {errors.branch && "Branch is required"}
              </span>
            </FormGroup>
            <FormGroup>
              <Label className="col-form-label pt-0">IBAN/Account Number</Label>
              <Input
                className="form-control"
                type="text"
                required=""
                name="account_no"
                innerRef={register({ required: true })}
              />
              <span style={{ color: "red" }}>
                {errors.account_no && "Account Number is required"}
              </span>
            </FormGroup>
            <FormGroup>
              <Label className="col-form-label pt-0">IFSC/SWIFT code</Label>
              <Input
                className="form-control"
                type="text"
                required=""
                name="ifsc"
                innerRef={register({ required: true })}
              />
              <span style={{ color: "red" }}>
                {errors.ifsc && "Swift Code is required"}
              </span>
            </FormGroup>
            <FormGroup>
              <Label className="col-form-label pt-0">Bank Name</Label>
              <Input
                className="form-control"
                type="text"
                required=""
                name="bank_name"
                innerRef={register({ required: true })}
              />
              <span style={{ color: "red" }}>
                {errors.bank_name && "Bank Name is required"}
              </span>
            </FormGroup>
            <Button
              color="primary btn-block"
              type="submit"
              style={{ maxWidth: 150 }}
            >
              Submit
            </Button>
          </Form>
        )}
      </CardBody>
    </Card>
  );
};

export default UserDetail;
