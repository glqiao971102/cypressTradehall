import React, { useState } from "react";
import DataTable from "react-data-table-component";
import moment from "moment";

const tableData = [
  {
    status: "Approved",
    date: moment().format("yyyy-mm-DD hh:MMA"),
    file: "1234",
  },
];

const columns = [
  {
    name: "Status",
    selector: "status",
    sortable: true,
  },
  {
    name: "Upload Date",
    selector: "date",
    sortable: true,
  },
  {
    name: "File Name",
    selector: "file",
    sortable: true,
  },
];

const IdentityTable = () => {
  const [data, setData] = useState(tableData);

  return (
    <DataTable
      noHeader
      data={data}
      columns={columns}
      striped={true}
      center={true}
    />
  );
};

export default IdentityTable;
