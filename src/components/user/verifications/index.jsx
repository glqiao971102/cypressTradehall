import React from "react";
import { Card, CardHeader, CardBody, Table } from "reactstrap";
import AddressTable from "./address_table";
import IdentityTable from "./identity_table";

const Verification = () => {
  return (
    <>
      <Card>
        <CardBody>
          <p style={{ margin: 0 }}>
            Your profile isn’t verified. Please verify your profile to take full
            advantage of our services!
          </p>
        </CardBody>
      </Card>
      <Card>
        <CardHeader>
          <h5>Identification</h5>
          <div className="p-1" />
          <p style={{ margin: 0 }}>Passport / National ID / Driving License</p>
          <div className="p-0" />
          <p style={{ margin: 0 }}>
            Please upload an identification document, in colour, where your full
            name is displayed.
          </p>
        </CardHeader>
        <CardBody>
          <IdentityTable />
        </CardBody>
      </Card>
      <Card>
        <CardHeader>
          <h5>Proof Of Address</h5>
          <div className="p-1" />
          <p style={{ margin: 0 }}>
            Proof of address that is not older than 180 days (utility bill /
            bank statement / other official document with your name and address
            on it)
          </p>
        </CardHeader>
        <CardBody>
          <AddressTable />
        </CardBody>
      </Card>
    </>
  );
};

export default Verification;
