import React from "react";
import {
  Card,
  CardBody,
  Badge,
  CardHeader,
  CardFooter,
  Row,
  Col,
  Button,
} from "reactstrap";

const Banner = ({ promotion }) => {
  return (
    <Card>
      <CardHeader className="p-0">
        <img
          src={promotion.image}
          alt={promotion.title}
          style={{ width: "100%", borderRadius: "12px 12px 0 0" }}
        />
      </CardHeader>
      <CardBody className="p-4">
        <h5>{promotion.title}</h5>
        <p>{promotion.description}</p>
        <Badge color={promotion.status === 0 ? "danger" : "success"} pill>
          {promotion.status === 0 ? "Unavailable" : "Available"}
        </Badge>
      </CardBody>
      <CardFooter className="p-0">
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Button color="primary" style={{ borderRadius: "0 0 0 12px", width: "50%" }}>
            {promotion.cta}
          </Button>
          <Button style={{ borderRadius: "0 0 12px 0", width: "50%" }}>
            Terms & Conditions
          </Button>
        </div>
      </CardFooter>
    </Card>
  );
};

export default Banner;
