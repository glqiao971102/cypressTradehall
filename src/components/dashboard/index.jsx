import React, { useState, useEffect } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";
import { useDispatch } from "react-redux";

import Breadcrumb from "../../layout/breadcrumb";
import Banner from "./banner";
// import DashboardCarousel from "./carousel";
import Wallet from "./wallet";
import Live from "./live";
import Demo from "./demo";
import Partner from "./partner";

import WalletService from "../../network/services/wallet";
import AccountService from "../../network/services/account";

const promotions = [
  {
    id: "1",
    title: "#Get Free Bonus",
    description:
      "Test your trading skills with free 30$ Welcome Bonus program. Join Now!",
    status: 0,
    terms: "https://via.placeholder.com/100x100?text=terms",
    image: "https://via.placeholder.com/300x150",
    cta: "Get Bonus!",
  },
  {
    id: "2",
    title: "#Challenge Yourself",
    description:
      "Get Ready For Demo Contest, next round will resumes at 01-May-20 - 01-June-20",
    status: 0,
    terms: "https://via.placeholder.com/100x100?text=terms",
    image: "https://via.placeholder.com/300x150",
    cta: "Join Now!",
  },
  {
    id: "3",
    title: "#Double the Deposits",
    description:
      "To increase your profit ratio.Deposit,choose the Bonus percentage and Earn more.",
    status: 1,
    terms: "https://via.placeholder.com/100x100?text=terms",
    image: "https://via.placeholder.com/300x150",
    cta: "Deposit Now!",
  },
];

const Dashboard = () => {
  const dispatch = useDispatch();
  const [activeTab, setActiveTab] = useState("wallet");

  const init = async () => {
    const walletResponse = await WalletService.get();
    dispatch({ type: "SET_WALLETS", wallets: walletResponse.wallets });
    dispatch({
      type: "SET_BALANCE",
      balance: walletResponse.wallet_balance,
    });
    dispatch({
      type: "SET_TOTAL_DEPOSIT",
      totalDeposit: walletResponse.total_deposit,
    });
    dispatch({
      type: "SET_TOTAL_WITHDRAWAL",
      totalWithdrawal: walletResponse.total_withdrawal,
    });
    dispatch({
      type: "SET_TOTAL_ACCOUNT_DEPOSIT",
      totalAccountDeposit: walletResponse.total_account_deposit,
    });
    dispatch({
      type: "SET_TOTAL_ACCOUNT_WITHDRAWAL",
      totalAccountWithdraw: walletResponse.total_account_withdraw,
    });

    const accountResponse = await AccountService.get();
    dispatch({ type: "SET_ACCOUNTS", accounts: accountResponse.accounts });
  };

  useEffect(() => {
    init();
    // eslint-disable-next-line
  }, []);

  return (
    <>
      <Breadcrumb title="Dashboard" />
      <Container fluid={true}>
        <Row>
          <Col>
            <Card>
              <CardBody className="p-4">
                <p style={{ margin: 0 }}>
                  Your profile isn’t verified. Please verify your profile to
                  take full advantage of our services!
                </p>
              </CardBody>
            </Card>
            <Card>
              <CardBody className="p-4">
                <p style={{ margin: 0 }}>Invite friends and earn money</p>
              </CardBody>
            </Card>
            {/* <Card>
              <CardBody className="cal-date-widget">
                <Row>
                  <Col>
                    <div className="cal-info text-center">
                      <h2>{moment().format("DD")}</h2>
                      <div className="d-inline-block mt-2">
                        <span className="b-r-dark pr-3">
                          {moment().format("MMM")}
                        </span>
                        <span className="pl-3">{moment().format("yyyy")}</span>
                      </div>
                      <p className="mt-4 f-16 text-muted">
                        Recent earnings: 0.00
                      </p>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card> */}
          </Col>
          {/* <Col>
            <DashboardCarousel />
          </Col> */}
        </Row>
        <Row>
          {promotions.map((promo) => {
            return (
              <Col key={promo.id}>
                <Banner promotion={promo} />
              </Col>
            );
          })}
        </Row>
        <div className="mb-3" />
        <Row>
          <Col md="12" className="project-list">
            <Card>
              <Row>
                <Col>
                  <Nav tabs className="border-tab">
                    <NavItem>
                      <NavLink
                        className={activeTab === "wallet" ? "active" : ""}
                        onClick={() => setActiveTab("wallet")}
                      >
                        <i className="icofont icofont-wallet"></i>
                        myWallet
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "live" ? "active" : ""}
                        onClick={() => setActiveTab("live")}
                      >
                        <i className="icofont icofont-bank"></i>
                        Live Account
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "demo" ? "active" : ""}
                        onClick={() => setActiveTab("demo")}
                      >
                        <i className="icofont icofont-bank-alt"></i>
                        Demo Account
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "partner" ? "active" : ""}
                        onClick={() => setActiveTab("partner")}
                      >
                        <i className="fa fa-users"></i>
                        Partner Account
                      </NavLink>
                    </NavItem>
                  </Nav>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>

        <TabContent activeTab={activeTab}>
          <TabPane tabId="wallet">
            <Wallet />
          </TabPane>
          <TabPane tabId="live">
            <Live />
          </TabPane>
          <TabPane tabId="demo">
            <Demo />
          </TabPane>
          <TabPane tabId="partner">
            <Partner />
          </TabPane>
        </TabContent>
      </Container>
    </>
  );
};

export default Dashboard;
