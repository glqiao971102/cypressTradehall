import React, { useEffect, useState } from "react";
import { Row, Col, Card, CardHeader, CardBody, Button } from "reactstrap";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";

import WalletTable from "./table";

import TransactionService from "../../../network/services/transaction";

const Wallet = () => {
  let history = useHistory();
  const wallets = useSelector((state) => state.wallet.wallets);
  const totalDeposit = useSelector((state) => state.wallet.totalDeposit);
  const totalWithdrawal = useSelector((state) => state.wallet.totalWithdrawal);
  const totalAccountDeposit = useSelector(
    (state) => state.wallet.totalAccountDeposit
  );
  const totalAccountWithdraw = useSelector(
    (state) => state.wallet.totalAccountWithdraw
  );
  const [transactions, setTransactions] = useState([]);

  const init = async () => {
    const transactionResponse = await TransactionService.get();
    setTransactions(transactionResponse.transactions);
  };

  useEffect(() => {
    init();
  }, []);

  return (
    <>
      {wallets.map((wallet) => {
        return (
          <Card className="card-absolute" key={wallet.id}>
            <CardHeader className="bg-primary">
              <h6 style={{ margin: 0 }}>
                {`#${wallet.wallet_code} - ${wallet.currency?.abbreviation}`}
              </h6>
            </CardHeader>
            <div className="card-right">
              <Button
                style={{ marginRight: 12, marginBottom: 12 }}
                onClick={() => {
                  history.push(`${process.env.PUBLIC_URL}/financial#deposit`);
                }}
              >
                Deposit
              </Button>
              <Button
                style={{ marginRight: 12, marginBottom: 12 }}
                onClick={() => {
                  history.push(
                    `${process.env.PUBLIC_URL}/financial#withdrawal`
                  );
                }}
              >
                Withdraw
              </Button>
              <Button style={{ marginBottom: 12 }}>Settings</Button>
            </div>
            <CardBody className="text-center">
              <Row>
                <Col className="abs-wallet">
                  <Row>
                    <Col>
                      <h6>{wallet.currency?.name}</h6>
                    </Col>
                  </Row>
                  <Row>
                    <Col>WALLET</Col>
                  </Row>
                </Col>
                <Col className="abs-wallet">
                  <Row>
                    <Col>
                      <h6>{`${wallet.balance} ${wallet.currency?.name}`}</h6>
                    </Col>
                  </Row>
                  <Row>
                    <Col>BALANCE</Col>
                  </Row>
                </Col>
                <Col className="abs-wallet">
                  <Row>
                    <Col>
                      <h6>{`${totalDeposit} ${wallet.currency?.name}`}</h6>
                    </Col>
                  </Row>
                  <Row>
                    <Col>TOTAL DEPOSITS</Col>
                  </Row>
                </Col>
                <Col className="abs-wallet">
                  <Row>
                    <Col>
                      <h6>{`${totalWithdrawal} ${wallet.currency?.name}`}</h6>
                    </Col>
                  </Row>
                  <Row>
                    <Col>TOTAL WITHDRAWALS</Col>
                  </Row>
                </Col>
                <Col className="abs-wallet">
                  <Row>
                    <Col>
                      <h6>{`${totalAccountDeposit} ${wallet.currency?.name}`}</h6>
                    </Col>
                  </Row>
                  <Row>
                    <Col>TOTAL MT5 ACCOUNT DEPOSIT</Col>
                  </Row>
                </Col>
                <Col className="abs-wallet">
                  <Row>
                    <Col>
                      <h6>{`${totalAccountWithdraw} ${wallet.currency?.name}`}</h6>
                    </Col>
                  </Row>
                  <Row>
                    <Col>TOTAL MT5 ACCOUNT WITHDRAWAL</Col>
                  </Row>
                </Col>
              </Row>
            </CardBody>
          </Card>
        );
      })}

      <Card>
        <CardBody>
          {transactions?.length > 0 ? (
            <WalletTable transactions={transactions} />
          ) : (
            <h5>No recent transactions</h5>
          )}
        </CardBody>
      </Card>
    </>
  );
};

export default Wallet;
