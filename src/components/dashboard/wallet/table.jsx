import React from "react";
import DataTable from "react-data-table-component";
import moment from "moment";

const columns = [
  {
    name: "Ticket",
    selector: "txn_id",
    sortable: true,
  },
  {
    name: "Date",
    selector: "updated_at",
    sortable: true,
    format: (row) => {
      return moment(row.updatedAt).format("yyyy-MM-DD HH:mmA");
    },
  },
  {
    name: "Wallet",
    selector: "currency_unit",
    sortable: true,
  },
  {
    name: "Action",
    selector: "txn_type",
    sortable: true,
    format: (row) => {
      return row.txn_type === 0
        ? "Deposit"
        : row.txn_type === 1
        ? "Withdraw"
        : "-";
    },
  },
  {
    name: "Method",
    selector: "method",
    sortable: true,
    format: (row) => {
      console.log(row)
      return row.txn_type === 0
        ? row?.deposit?.payment_method
        : row.txn_type === 1
        ? row?.withdraw?.payment_method
        : "-";
    },
  },
  {
    name: "Amount",
    selector: "debit_amount",
    sortable: true,
    format: (row) => {
      return row.debit_amount > 0 ? row.debit_amount : row.credit_amount;
    },
  },
  {
    name: "Status",
    selector: "status",
    sortable: true,
    format: (row) => {
      switch (row.status) {
        case 0:
          return "Pending";
        case 1:
          return "Success";
        case 2:
          return "Failed";
        default:
          return "-";
      }
    },
  },
];

const WalletTable = ({ transactions }) => {
  return (
    <DataTable
      title="Recent transactions"
      data={transactions}
      columns={columns}
      striped={true}
      center={true}
    />
  );
};

export default WalletTable;
