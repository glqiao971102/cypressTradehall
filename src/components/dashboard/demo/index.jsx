import React from "react";
import { Row, Col, Card, CardHeader, CardBody, Button } from "reactstrap";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";

const Demo = () => {
  let history = useHistory();
  const demoAccounts = useSelector((state) =>
    state.account.accounts.filter((e) => e.account_type === 1)
  );
  const currencies = useSelector((state) => state.currency.currencies);

  return (
    <>
      <Card className="card-absolute">
        {demoAccounts?.length > 0 ? (
          demoAccounts.map((account) => {
            const currency = currencies.find(
              (e) => e.id === account.currency_id
            );
            return (
              <>
                <CardHeader className="bg-secondary">
                  <h6 style={{ margin: 0 }}>
                    {`#${account.account_code} ${account.plan?.name} - DEMO`}
                  </h6>
                </CardHeader>
                <div className="card-right">
                  <Button
                    style={{ marginRight: 12, marginBottom: 12 }}
                    onClick={() => {
                      history.push(`${process.env.PUBLIC_URL}/financial`);
                    }}
                  >
                    Deposit
                  </Button>
                  <Button style={{ marginBottom: 12 }}>Settings</Button>
                </div>
                <CardBody className="text-center">
                  <Row>
                    <Col className="abs-wallet">
                      <Row>
                        <Col>
                          <h6>{account.plan?.name}</h6>
                        </Col>
                      </Row>
                      <Row>
                        <Col>ACCOUNT TYPE</Col>
                      </Row>
                    </Col>
                    <Col className="abs-wallet">
                      <Row>
                        <Col>
                          <h6>1:{account.leverage}</h6>
                        </Col>
                      </Row>
                      <Row>
                        <Col>LEVERAGE</Col>
                      </Row>
                    </Col>
                    <Col className="abs-wallet">
                      <Row>
                        <Col>
                          <h6>{`${account.balance} ${
                            currency?.name ?? ""
                          }`}</h6>
                        </Col>
                      </Row>
                      <Row>
                        <Col>BALANCE</Col>
                      </Row>
                    </Col>
                    <Col className="abs-wallet">
                      <Row>
                        <Col>
                          <h6>{`${account.credit} ${currency?.name ?? ""}`}</h6>
                        </Col>
                      </Row>
                      <Row>
                        <Col>CREDIT</Col>
                      </Row>
                    </Col>
                    <Col className="abs-wallet">
                      <Row>
                        <Col>
                          <h6>{`${account.equity} ${currency?.name ?? ""}`}</h6>
                        </Col>
                      </Row>
                      <Row>
                        <Col>EQUITY</Col>
                      </Row>
                    </Col>
                    <Col className="abs-wallet">
                      <Row>
                        <Col>
                          <h6>{`${account.free_margin} ${
                            currency?.name ?? ""
                          }`}</h6>
                        </Col>
                      </Row>
                      <Row>
                        <Col>FREE MARGIN</Col>
                      </Row>
                    </Col>
                    <Col className="abs-wallet">
                      <Row>
                        <Col>
                          <h6>{`${account.stop_risk} ${
                            currency?.name ?? ""
                          }`}</h6>
                        </Col>
                      </Row>
                      <Row>
                        <Col>STOP RISK</Col>
                      </Row>
                    </Col>
                    <Col className="abs-wallet">
                      <Row>
                        <Col>
                          <h6>-</h6>
                        </Col>
                      </Row>
                      <Row>
                        <Col>AGENT CODE</Col>
                      </Row>
                    </Col>
                  </Row>
                </CardBody>
              </>
            );
          })
        ) : (
          <CardBody className="text-center">No active accounts</CardBody>
        )}
      </Card>
    </>
  );
};

export default Demo;
