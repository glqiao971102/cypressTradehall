import React, { useState } from "react";
import { Container, Card, CardBody, Col, Row } from "reactstrap";
import StepZilla from "react-stepzilla";

import Breadcrumb from "../../layout/breadcrumb";
import DemoForm from "./form";
import DemoAccountList from "./list";

const DemoAccounts = () => {
  const [stage, setStage] = useState(0);

  return (
    <>
      <Breadcrumb title="Demo Accounts" />
      <Container fluid={true}>
        <Card>
          <CardBody>
            <h5>Demo Trader Account Registration</h5>
            <div className="p-2" />
            <Row>
              <Col
                className={`u-pearl col-6 ${stage === 0 && "current"} ${
                  stage > 0 && "done"
                }`}
              >
                <span className="u-pearl-number">1</span>
                <span className="u-pearl-title">Select account type</span>
              </Col>
              <Col
                className={`u-pearl col-6 ${stage === 1 && "current"} ${
                  stage > 1 && "done"
                }`}
              >
                <span className="u-pearl-number">2</span>
                <span className="u-pearl-title">Create account</span>
              </Col>
            </Row>
          </CardBody>
        </Card>
        <StepZilla
          steps={[
            {
              name: "Step 1",
              component: <DemoAccountList />,
            },
            {
              name: "Step 2",
              component: <DemoForm />,
            },
          ]}
          showSteps={false}
          onStepChange={(index) => {
            setStage(index);
          }}
        />
        <div className="p-5" />
      </Container>
    </>
  );
};

export default DemoAccounts;
