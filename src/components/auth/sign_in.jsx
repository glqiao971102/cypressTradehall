import React, { useState } from "react";
import { Form, FormGroup, Input, Label, Button } from "reactstrap";
import { useForm } from "react-hook-form";

import AuthService from "../../network/services/auth";

const SignInForm = () => {
  const { register, handleSubmit, errors } = useForm();
  const [loginError, setLoginError] = useState(null);

  const submitAuth = async (data) => {
    try {
      const result = await AuthService.login({
        email: data.email,
        password: data.password,
      });

      console.log(result);

      if (result.success) {
        localStorage.setItem("token", result.data.jwt.token);
        localStorage.setItem("user", JSON.stringify(result.data.user));
        window.location.reload();
      }
    } catch (error) {
      console.log(error);
      if (error.message === "E_INVALID_AUTH_PASSWORD" || error.message === "E_INVALID_AUTH_UID") {
        setLoginError("Invalid credential");
      } else {
        setLoginError(error.message ?? "Please try again later");
      }
    }
  };

  return (
    <div>
      <Form
        className="theme-form"
        onSubmit={handleSubmit(submitAuth)}
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
        }}
      >
        <h4 className="text-center">LOGIN</h4>
        <FormGroup>
          <Label className="col-form-label pt-0">Email</Label>
          <Input
            className="form-control"
            type="text"
            required=""
            name="email"
            innerRef={register({ required: true })}
          />
          <span style={{ color: "red" }}>
            {errors.email && "Email is required"}
          </span>
        </FormGroup>
        <FormGroup>
          <Label className="col-form-label">Password</Label>
          <Input
            className="form-control"
            type="password"
            required=""
            name="password"
            innerRef={register({ required: true })}
          />
          <span style={{ color: "red" }}>
            {errors.password && "Password is required"}
          </span>
        </FormGroup>
        {loginError != null && (
          <span style={{ color: "red" }}>{loginError}</span>
        )}
        <FormGroup className="form-row mt-3 mb-0">
          <Button color="primary btn-block">LOGIN</Button>
        </FormGroup>
        <div className="placeholder-height" />
      </Form>
    </div>
  );
};

export default SignInForm;
