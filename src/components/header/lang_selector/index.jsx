import React, { useState } from "react";
import {
  setTranslations,
  setDefaultLanguage,
  setLanguageCookie,
  setLanguage,
} from "react-switch-lang";

import en from "../../../assets/i18n/en.json";
import cn from "../../../assets/i18n/cn.json";

setTranslations({ en, cn });
setDefaultLanguage("en");
setLanguageCookie();

const LangSelector = () => {
  const [langdropdown, setLangdropdown] = useState(false);
  const [selected, setSelected] = useState("en");

  const handleSetLanguage = (key) => {
    setLanguage(key);
    setSelected(key);
  };

  const LanguageSelection = (language) => {
    if (language) {
      setLangdropdown(!language);
    } else {
      setLangdropdown(!language);
    }
  };

  return (
    <div className={`translate_wrapper ${langdropdown ? "active" : ""}`}>
      <div className="current_lang">
        <div className="lang" onClick={() => LanguageSelection(langdropdown)}>
          <i
            className={`flag-icon flag-icon-${
              selected === "en" ? "us" : selected
            }`}
          ></i>
          <span className="lang-txt">{selected}</span>
        </div>
      </div>
      <div className={`more_lang ${langdropdown ? "active" : ""}`}>
        <div className="lang" onClick={() => handleSetLanguage("en")}>
          <i className="flag-icon flag-icon-us"></i>
          <span className="lang-txt">
            English<span> (US)</span>
          </span>
        </div>
        <div className="lang" onClick={() => handleSetLanguage("cn")}>
          <i className="flag-icon flag-icon-cn"></i>
          <span className="lang-txt">简体中文</span>
        </div>
      </div>
    </div>
  );
};

export default LangSelector;
