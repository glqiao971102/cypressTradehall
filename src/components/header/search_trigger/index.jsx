import React, { useEffect } from "react";
import { Search as SearchIcon } from "react-feather";
import { useSelector, useDispatch } from "react-redux";

import { TRIGGER_SEARCH } from "../../../redux/actionTypes";

const SearchTrigger = () => {
  const isSearching = useSelector((state) => state.header.isSearching);
  const dispatch = useDispatch();

  useEffect(() => {
    if (isSearching) {
      document.querySelector(".search-full").classList.add("open");
      document.querySelector(".more_lang").classList.remove("active");
    } else {
      document.querySelector(".search-full").classList.remove("open");
    }
  }, [isSearching]);

  return (
    <span className="header-search">
      <SearchIcon
        onClick={() => {
          dispatch({ type: TRIGGER_SEARCH });
        }}
      />
    </span>
  );
};

export default SearchTrigger;
