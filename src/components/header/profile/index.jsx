import React, { useState, useEffect } from "react";
import { FileText, LogIn, Mail, User } from "react-feather";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

import Auth from "../../../network/services/auth";

const Profile = () => {
  const currentUser = useSelector((state) => state.user.user);
  const logOut = () => {
    Auth.logout();
  };

  return (
    <>
      <div className="media profile-media">
        {/* <img className="b-r-10" src={man} alt="" /> */}
        <div className="media-body">
          <span>{currentUser?.full_name ?? "Name"}</span>
          <p className="mb-0 font-roboto">
            {currentUser?.email ?? "Email"}{" "}
            <i className="middle fa fa-angle-down"></i>
          </p>
        </div>
      </div>
      <ul className="profile-dropdown onhover-show-div">
        <li>
          <Link to={`${process.env.PUBLIC_URL}/account`} href="#javascript">
            <User />
            <span>Account</span>
          </Link>
        </li>
        {/* <li>
          <Mail />
          <span>Inbox</span>
        </li> */}
        {/* <li>
          <FileText />
          <span>Taskboard</span>
        </li> */}
        <li onClick={logOut}>
          <LogIn />
          <span>Log Out</span>
        </li>
      </ul>
    </>
  );
};

export default Profile;
