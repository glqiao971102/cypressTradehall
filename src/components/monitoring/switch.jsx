import React, { useState, useEffect } from "react";
import { Button } from "reactstrap";
import { useSelector, useDispatch } from "react-redux";

import { TOGGLE_MONITORING_PRESENTATION } from "../../redux/actionTypes";

const SwitchButton = ({ options }) => {
  const dispatch = useDispatch();
  const isGraphical = useSelector((state) => state.monitoring.isGraphical);

  return (
    <>
      <Button
        color="primary"
        outline={isGraphical ? false : true}
        onClick={() => {
          dispatch({ type: TOGGLE_MONITORING_PRESENTATION });
        }}
      >
        Graphical
      </Button>
      <Button
        color="primary"
        outline={!isGraphical ? false : true}
        onClick={() => {
          dispatch({ type: TOGGLE_MONITORING_PRESENTATION });
        }}
        style={{ marginLeft: 8 }}
      >
        Transaction
      </Button>
    </>
  );
};

export default SwitchButton;
