import React, { useState, useEffect } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  Col,
  Table,
  Row,
  Button,
} from "reactstrap";
import moment from "moment";

const MonitoringGraphical = () => {
  return (
    <Row>
      <Col sm={6}>
        <Card>
          <CardHeader style={{ padding: 20 }} className="bg-primary">
            <h5 className="text-center">Account stats</h5>
          </CardHeader>
          <CardBody style={{ padding: 16 }}>
            <Table borderless>
              <tbody>
                <tr>
                  <td className="text-right" width="50%">
                    Current Balance, USC :
                  </td>
                  <td>0</td>
                </tr>
                <tr>
                  <td className="text-right">Current Equity, USC :</td>
                  <td>0</td>
                </tr>
                <tr>
                  <td className="text-right">Total Account Profitability :</td>
                  <td>0</td>
                </tr>
                <tr>
                  <td className="text-right">Open Orders Count :</td>
                  <td>0</td>
                </tr>
              </tbody>
            </Table>
          </CardBody>
        </Card>
      </Col>
      <Col sm={6}>
        <Card>
          <CardHeader style={{ padding: 20 }} className="bg-primary">
            <h5 className="text-center">Account data</h5>
          </CardHeader>
          <CardBody style={{ padding: 16 }}>
            <Table borderless>
              <tbody>
                <tr>
                  <td className="text-right" width="50%">
                    ACCOUNT :
                  </td>
                  <td>51423</td>
                </tr>
                <tr>
                  <td className="text-right">ACCOUNT HOLDER :</td>
                  <td>Name</td>
                </tr>
                <tr>
                  <td className="text-right">REGISTRATION DATE :</td>
                  <td>{moment().format("yyyy-mm-DD hh:MMA")}</td>
                </tr>
                <tr>
                  <td className="text-right">ACCOUNT TYPE :</td>
                  <td>CENT</td>
                </tr>
                <tr>
                  <td className="text-right">COUNTRY :</td>
                  <td>MALAYSIA</td>
                </tr>
                <tr>
                  <td className="text-right">CURRENCY :</td>
                  <td>USD</td>
                </tr>
                <tr>
                  <td className="text-right">LEVERAGE :</td>
                  <td>1:50</td>
                </tr>
              </tbody>
            </Table>
          </CardBody>
        </Card>
      </Col>
    </Row>
  );
};

export default MonitoringGraphical;
