import React from "react";
import { Container, Row, Col, Card, CardHeader, CardBody } from "reactstrap";
import { useSelector } from "react-redux";

import Breadcrumb from "../../layout/breadcrumb";
import MonitoringOpenTable from "./table/open";
import MonitoringClosedTable from "./table/closed";
import ColorDropdown from "./dropdown";
import SwitchButton from "./switch";
import MonitoringGraphical from "./graphical";

const Monitoring = (props) => {
  const isGraphical = useSelector((state) => state.monitoring.isGraphical);

  return (
    <>
      <Breadcrumb title="Monitoring" />
      <Container fluid={true}>
        <Row>
          <Col sm="12">
            <Card>
              <CardHeader>
                <h5>Monitoring</h5>
                <div className="p-10"></div>
                <Row>
                  <Col>
                    <ColorDropdown />
                  </Col>
                  <Col className="text-right">
                    <SwitchButton options={["Graphical", "Transaction"]} />
                  </Col>
                </Row>
              </CardHeader>
              <CardBody>
                {isGraphical ? (
                  <>
                    <MonitoringGraphical />
                  </>
                ) : (
                  <>
                    <MonitoringOpenTable />
                    <MonitoringClosedTable />
                  </>
                )}
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Monitoring;
