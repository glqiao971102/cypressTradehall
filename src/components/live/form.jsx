import React, { useState } from "react";
import {
  Row,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  Card,
  CardBody,
} from "reactstrap";
import { useForm } from "react-hook-form";

const LiveForm = () => {
  const { register, handleSubmit, errors } = useForm();

  const handleCreateAccount = (data) => {
    if (data !== "") {
    } else {
      errors.showMessages();
    }
  };

  return (
    <>
      <Card>
        <CardBody>
          <Form
            className="theme-form"
            onSubmit={handleSubmit(handleCreateAccount)}
          >
            <Row>
              <Col>
                <FormGroup>
                  <Label>Account Type</Label>
                  <Input
                    className="form-control"
                    type="text"
                    name="type"
                    defaultValue="VIP1"
                    innerRef={register({ required: true })}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col>
                <FormGroup>
                  <Label>Currency</Label>
                  <Input
                    type="select"
                    name="currency"
                    className="form-control digits"
                    innerRef={register({ required: true })}
                  >
                    <option value="usd">USD</option>
                  </Input>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col>
                <FormGroup>
                  <Label>Leverage</Label>
                  <Input
                    type="select"
                    name="leverage"
                    className="form-control digits"
                    innerRef={register({ required: true })}
                  >
                    <option value="10">10</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                  </Input>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col>
                <FormGroup>
                  <Label>Introducing Broker</Label>
                  <Input
                    type="select"
                    name="ib_select"
                    className="form-control digits"
                    innerRef={register({ required: true })}
                  >
                    <option value="none">No Introducing Broker</option>
                    <option value="exist">Existing Introducing Broker</option>
                  </Input>
                </FormGroup>
              </Col>
              <Col>
                <FormGroup>
                  <Label>IB Code</Label>
                  <Input
                    className="form-control"
                    type="text"
                    name="message"
                    innerRef={register({ required: true })}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col>
                <FormGroup>
                  <Label>Stop Risk Percentage</Label>
                  <Input
                    type="select"
                    name="ib_select"
                    className="form-control digits"
                    innerRef={register({ required: true })}
                  >
                    <option value="100">100%</option>
                    <option value="50">50%</option>
                  </Input>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col>
                <FormGroup className="mb-0">
                  <Button color="success" className="mr-3">
                    Confirm
                  </Button>
                </FormGroup>
              </Col>
            </Row>
          </Form>
        </CardBody>
      </Card>
      <Card>
        <CardBody>
          <h5>Trading Conditions</h5>
          <ol style={{ display: "flex", flexWrap: "wrap" }}>
            <li style={{ width: "33.33%" }}>
              Recommended capital - from 50000
            </li>
            <li style={{ width: "33.33%" }}>Min. lot 0.01</li>
            <li style={{ width: "33.33%" }}>Max. lot 500</li>
            <li style={{ width: "33.33%" }}>Unlimited Open orders</li>
            <li style={{ width: "33.33%" }}>No Limit order history</li>
            <li style={{ width: "33.33%" }}>Market execution</li>
            <li style={{ width: "33.33%" }}>Fixed spread from 0.1 Points</li>
            <li style={{ width: "33.33%" }}>From 2.5$ per lot</li>
            <li style={{ width: "33.33%" }}>
              Limit & stop levels from No Limit
            </li>
          </ol>
        </CardBody>
      </Card>
    </>
  );
};

export default LiveForm;
