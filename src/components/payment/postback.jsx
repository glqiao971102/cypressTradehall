import React from "react";

const PaymentPostback = () => {
  return (
    <div>
      <h5>Payment loading...</h5>
    </div>
  );
};

export default PaymentPostback;
